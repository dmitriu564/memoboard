import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../context/auth.context";
import { useHttp, ValidationError } from "../hooks/http.hook";
import { Post } from "./MemoCard";

interface CreateMemoProps {
    closeHandler: React.MouseEventHandler<HTMLButtonElement>;
    reloadHandler: Function;
}

export const CreateMemo = ({ closeHandler, reloadHandler }: CreateMemoProps) => {
    const auth = useContext(AuthContext);
    const { loading, err, request } = useHttp();
    const [post, setPost] = useState<Post>({ title: '', description: '', date: Date() });
    const [isSuccess, setSuccess] = useState(false);
    const [validity, setValidity] = useState({
        title: true, description: true
    });

    const [file, setFile] = useState<File>();

    const changeHandler = (e: React.FormEvent<HTMLInputElement>): void => {
        setPost({ ...post, [e.currentTarget.name]: e.currentTarget.value });
        setValidity({ ...validity, [e.currentTarget.name]: true });
    }

    const addHandler = async () => {
        try {
            setSuccess(false);
            let toAdd = {...post}
            if(file)
            {
                let data = new FormData();
                data.append('file', file);
                const image = await request('http://localhost:5000/api/v1/images', 'POST', data);
                toAdd = {...toAdd, imageId: image.id};
            }
            const res = await request('/api/v1/posts', 'POST', toAdd, {
                'Authorization': `Bearer ${auth.token}`,
                'Content-Type': 'application/json'
            });
            if (res) {
                setSuccess(true);
                reloadHandler();
            }
        } catch (e) {
            //console.log(e.message);
        }
    }

    const fileHandler: React.ChangeEventHandler<HTMLInputElement> = async (e) => {
        try {
            if (e.currentTarget.files) {
                setFile(e.currentTarget.files[0]);
            }
        } catch (e) {
            console.log(e.message);
        }

    }

    useEffect(() => {
        M.updateTextFields()
    }, [])

    useEffect(() => {
        setValidity({
            title: true,
            description: true
        });
        if (err) {
            const valErr = (err as ValidationError);
            setValidity({
                title: !valErr.title,
                description: !valErr.description,
            });
        }
    }, [err]);

    return (
        <div className="container top-padded">
            <div className="card grey lighten-2">
                <div className="card-content grey lighten-3">
                    <div className="card-content black-text">
                        <div className="input-field">
                            <input
                                className={validity.title
                                    ? isSuccess ? 'valid' : ''
                                    : 'invalid'}
                                placeholder="Title"
                                id="title"
                                name="title"
                                type="text"
                                value={post.title}
                                onChange={changeHandler}
                            />
                            <label htmlFor="title">Title</label>
                            <span className={validity.title ? '' : 'section helper-text'} data-error={(err as ValidationError)?.title}></span>
                        </div>
                        <div className="input-field">
                            <input
                                className={validity.description
                                    ? isSuccess ? 'valid' : ''
                                    : 'invalid'}
                                placeholder="Description"
                                id="description"
                                name="description"
                                type="text"
                                value={post.description}
                                onChange={changeHandler}
                            />
                            <label htmlFor="description">Description</label>
                            <span className={validity.description ? '' : 'section helper-text'} data-error={(err as ValidationError)?.description}></span>
                        </div>
                        <div className="input-field">
                            <input
                                className={isSuccess ? 'valid' : ''}
                                id="date"
                                name="date"
                                type="date"
                                value={post.date}
                                onChange={changeHandler}
                            />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="file-field input-field">
                            <div className="btn">
                                <span>Image</span>
                                <input type="file" onChange={fileHandler} />
                            </div>
                            <div className="file-path-wrapper">
                                <input className="file-path" type="text" />
                            </div>
                        </div>
                    </div>
                    <div className="card-action">
                        <button
                            className="btn green darken-1 waves-effect waves-light"
                            onClick={addHandler}
                            disabled={loading}
                        >Add Memo</button>
                        <button
                            className="right btn red lighten-2 waves-effect waves-light"
                            onClick={closeHandler}
                            disabled={loading}
                        >Close</button>
                    </div>
                </div>
            </div>

        </div>





    );
}