import { useCallback, useEffect, useState } from "react"
import {decode} from 'jsonwebtoken'

export interface User {
    _id: string;
    email: string;
    password: string;
    gender: string;
    name: string;
}
const storage = 'uData';

export const useAuth = () => {
    const [token, setToken] = useState<string | null>(null);
    const [user, setUser] = useState<User | null>(null);

    const login = useCallback((jwt: string, user: User)=>{
        setToken(jwt);
        setUser(user);
        localStorage.setItem(storage, JSON.stringify({
            user: user, token: jwt
        }));
    }, []);

    const logout = useCallback(()=>{
        setToken(null);
        setUser(null);
        localStorage.removeItem(storage);
    }, []);
    
    useEffect(()=>{
        const data = JSON.parse(localStorage.getItem(storage) as string);
        if(data && data.token && data.user)
        {
            const decoded: any = decode(data.token);
            if (decoded && Date.now() <= parseInt(decoded.exp, 10)*1000) 
                login(data.token, data.user);
            else 
                localStorage.removeItem(storage);

            
        }
    },[login]);

    return {login, logout, token, user};
}