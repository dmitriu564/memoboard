interface PaginationData {
    pagged: any[];
    pages_count: number;
}

export const paginate = (array: any[], page_size: number = 10, page_number: number = 0): PaginationData => {
    if(array.length === 0 || !array.slice) {
        return {
            pagged: [],
            pages_count: 0
        }
    }
    if (page_number <= 0) {
        return {
            pagged: array.slice(0, page_size),
            pages_count: 1
        };
    }
    if (page_size <= 0 || page_size >= 100)
        throw RangeError("Bad arguments");
    return {
        pagged: array.slice((page_number - 1) * page_size, page_number * page_size),
        pages_count: Math.ceil(array.length / page_size)
    };
}