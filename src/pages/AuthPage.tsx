import React, { useEffect, useState } from 'react';
import { useAuth } from '../hooks/auth.hook';
import { useHttp, ValidationError } from '../hooks/http.hook';

export const AuthPage = () => {
    const auth = useAuth();
    const { loading, err, request } = useHttp();
    const [form, setForm] = useState({
        name: "",
        email: "",
        password: "",
        gender: "unicorn",
    });

    const [validity, setValidity] = useState({
        name: true,
        email: true,
        password: true,
    });

    const onInputChange = (e: React.FormEvent<HTMLInputElement>): void => {
        setForm({ ...form, [e.currentTarget.name]: e.currentTarget.value });
        setValidity({...validity, [e.currentTarget.name]: true });
    }

    const onSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setForm({ ...form, "gender": e.currentTarget.value });
    }

    const regHandler = async () => {
        try {
            const data = await request(
                '/api/v1/auth/signup', 
                'POST', 
                { ...form }, {'Content-Type': 'application/json'});
            auth.login(data.token, data.user);
            window.location.reload(); //TODO: find a better solution
        }
        catch (e) {console.log(e.message)};
    }

    const loginHandler = async () => {
        try {
            const data = await request(
                '/api/v1/auth/login', 
                'POST',{
                    username: form.email, 
                    password: form.password 
                }, {'Content-Type': 'application/json'});
            auth.login(data.token, data.user);
            window.location.reload();
        }
        catch(e) {console.log(e.message)};
    }

    const updateFields = () => {
        M.updateTextFields();
    }

    useEffect(()=> {
        M.AutoInit();
        updateFields();
      }, []);

    useEffect(()=> {
        setValidity({
            name: true,
            email: true,
            password: true,
        });
        if(err){
            const valErr = (err as ValidationError);
            setValidity({
                name: !valErr.name,
                email: !valErr.email,
                password: !valErr.password,
            });
        }
    }, [err]);

    return (
        <div className="row">
            <div className="col s6 offset-s3">
                <h1>
                    Memoboard
                </h1>

                <div className="card grey lighten-2">
                    <div className="card-tabs">
                        <ul className="tabs tabs-fixed-width" onClick={updateFields}>
                            <li className="tab"><a href="#login">Log in</a></li>
                            <li className="tab"><a className="active" href="#signup">Sign Up</a></li>
                        </ul>
                    </div>

                    <div className="card-content grey lighten-3">
                        <div id="login">
                            <div className="card-content black-text">
                                <div className="input-field">
                                    <input
                                        className="validate"
                                        id="email_login"
                                        type="text"
                                        name="email"
                                        value={form.email}
                                        onChange={onInputChange}
                                        autoComplete="new-password"
                                    />
                                    <label htmlFor="email_login">Email</label>
                                </div>

                                <div className="input-field">
                                    <input
                                        className="validate"
                                        id="password_login"
                                        type="password"
                                        name="password"
                                        value={form.password}
                                        onChange={onInputChange}
                                        autoComplete="new-password"
                                    />
                                    <label htmlFor="password_login">Password</label>
                                </div>
                            </div>
                            <div className="card-action">
                                <button
                                    className="waves-effect waves-teal btn-flat"
                                onClick={loginHandler}
                                disabled={loading}
                                >Log in</button>
                            </div>
                        </div>
                        <div id="signup">
                            <div className="card-content black-text">
                                <div className="input-field">
                                    <input
                                        className={validity.email?'':'invalid'}
                                        id="email"
                                        type="text"
                                        name="email"
                                        value={form.email}
                                        onChange={onInputChange}
                                        autoComplete="new-password"
                                    />
                                    <label htmlFor="email">Email</label>
                                    <span className={validity.email?'':'section helper-text'} data-error={(err as ValidationError)?.email}></span>
                                </div>

                                <div className="input-field">
                                    <input
                                        className={validity.password?'':'invalid'}
                                        id="password"
                                        type="password"
                                        name="password"
                                        value={form.password}
                                        onChange={onInputChange}
                                        autoComplete="new-password"
                                    />
                                    <label htmlFor="password">Password</label>
                                    <span className={validity.password?'':'section helper-text'} data-error={(err as ValidationError)?.password}></span>
                                </div>

                                <div className="input-field">
                                    <input
                                        className={validity.name?'':'invalid'}
                                        id="fullname"
                                        type="text"
                                        name="name"
                                        value={form.name}
                                        onChange={onInputChange}
                                        autoComplete="new-password"
                                    />
                                    <label htmlFor="fullname">Full Name</label>
                                    <span className={validity.name?'':'section helper-text'} data-error={(err as ValidationError)?.name}></span>
                                </div>

                                <div className="input-field">
                                    <select onChange={onSelectChange} defaultValue="unicorn">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                        <option value="unicorn">Unicorn</option>
                                    </select>
                                    <label>Gender</label>
                                </div>
                            </div>
                            <div className="card-action">
                                <button
                                    className="btn darken-1 waves-effect waves-light"
                                onClick={regHandler}
                                disabled={loading}
                                >Sign up</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}