import React, { useCallback, useEffect, useState } from "react";
import ReactModal from 'react-modal';
import { paginate } from "../utils/paginator";
import { CreateMemo } from "./CreateMemo";
import { MemoCard, PostWithId } from "./MemoCard";
import { PaginationRow } from "./PaginationRow";
import { Search } from "./Search";

interface MemoListProps {
    memos: PostWithId[];
    deleteHandler: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, id: string) => void;
    loading: boolean;
    reloadHandler: Function;
}



export const MemoList = ({ memos, deleteHandler, loading, reloadHandler }: MemoListProps) => {
    const [renderedMemos, setRenderedMemos] = useState<PostWithId[]>();
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(0);
    const [pagged, setPagged] = useState<PostWithId[]>([]);
    const [isAddModal, setAddModal] = useState(false);
    const [filters, setFilters] = useState({ isUpcoming: false, isNewOld: true});
    const [searchText, setSearchText] = useState('');

    const pageHandler = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, i: number) => {
        e.preventDefault();
        setPage(i);
    }

    const searchHandler = useCallback(async (searchTxt: string) => {
        setSearchText(searchTxt);
    }, [memos, setRenderedMemos]);

    useEffect(() => {
        if (searchText === '') {
            setRenderedMemos(memos);
        }
        else {
            const filteredMemos =
                memos.filter(
                    (memo) => {
                        return (memo.description.indexOf(searchText) > -1) || (memo.title.indexOf(searchText) > -1);
                    });
            setRenderedMemos(filteredMemos);
        }
        if(!filters.isNewOld)
            setRenderedMemos((rendered) => rendered?.slice().reverse());
        if(filters.isUpcoming)
            setRenderedMemos((rendered) => {
                return rendered?.filter(
                    (memo) => {
                        return Date.parse(memo.date) > Date.now();
                    })
            });
    },[filters, searchText, memos]);

    const filtersHandler: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setFilters({...filters, [e.currentTarget.name]: e.currentTarget.checked});
    };

    useEffect(() => {
        console.log(renderedMemos);
        const { pagged, pages_count } = renderedMemos ? paginate(renderedMemos, 10, page) : { pagged: [], pages_count: 0 };
        setPagged(pagged);
        setTotalPages(pages_count);
    }, [page, renderedMemos, setPagged, setTotalPages, memos]);


    return (
        <>
            <div className="fixed-action-btn">
                <a className="btn-floating btn-large green lighten-1" onClick={() => { setAddModal(true) }}>
                    <i className="large material-icons">add</i>
                </a>
            </div>

            <Search searchHandler={searchHandler} />

            <form>
                <p>
                    <label>
                        <input type="checkbox" name="isNewOld" checked={filters.isNewOld} onChange={filtersHandler}/>
                        <span>{filters.isNewOld?"Newer first":"Older first"}</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input type="checkbox" name="isUpcoming" checked={filters.isUpcoming} onChange={filtersHandler}/>
                        <span>Show only upcoming memos</span>
                    </label>
                </p>
            </form>

            <ReactModal
                isOpen={isAddModal}
                contentLabel="Add new memo"
                onRequestClose={() => { setAddModal(false) }}
                className="modal-transparent"
            >
                <CreateMemo closeHandler={(e) => { setAddModal(false) }} reloadHandler={reloadHandler} />
            </ReactModal>

            <div className={"col " + (!(renderedMemos && renderedMemos.length > 0) && "center")}>
                {(pagged && pagged.length > 0)
                    ? pagged.map((memo) => <MemoCard key={memo._id} memo={memo} onDelete={deleteHandler} loading={loading} />)
                    : (!loading && "No memos")}
            </div>

            {(renderedMemos && renderedMemos.length > 0) && <PaginationRow totalPages={totalPages} onSetPage={pageHandler} />}

        </>
    );
}