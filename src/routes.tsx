import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { AboutPage } from './pages/AboutPage';
import { AuthPage } from './pages/AuthPage';
import { ContactUsPage } from './pages/ContactUsPage';
import { MainPage } from './pages/Main Page';

export const useRoutes = (isAuthed: boolean) => {
    if(isAuthed)
    {
        return (
            <Switch>
                <Route path="/events" exact>
                    <MainPage/>
                </Route>
                <Route path="/about" exact>
                    <AboutPage/>
                </Route>
                <Route path="/contactus" exact>
                    <ContactUsPage/>
                </Route>
                <Redirect to="/events"/>
            </Switch>
        );
    }

    return (
        <Switch>
            <Route path="/" exact>
                <AuthPage />
            </Route>
            <Redirect to="/"/>
        </Switch>
    );
}