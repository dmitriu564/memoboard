import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop({ required: true, unique: true })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop()
  gender: string;

  @Prop()
  name: string;
}

export interface IUser {
  email: string;
  password: string;
  gender: string;
  name: string;
}

export const UserDocument = SchemaFactory.createForClass(User);
