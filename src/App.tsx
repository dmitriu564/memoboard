import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { useRoutes } from './routes';
import './scss/materialize.scss';
import './scss/style.scss';
import { useAuth } from './hooks/auth.hook';
import * as M from 'materialize-css';
import { AuthContext } from './context/auth.context';
import { Navbar } from './components/Navbar';
import ReactModal from 'react-modal';

function App() {
  const {token, login, logout, user} = useAuth();
  const isAuthed = !!token;
  const routes = useRoutes(isAuthed);

  useEffect(()=> {
      M.AutoInit();
      M.updateTextFields();
      ReactModal.setAppElement('#root');
    }, []);


  return (
    <AuthContext.Provider value={{token, user, login, logout, isAuthed}}>
      <BrowserRouter>
      {isAuthed && <Navbar/>}
        <div className="">
          {routes}
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
