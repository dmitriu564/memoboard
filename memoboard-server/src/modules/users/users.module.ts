import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserDocument } from './user.entity';
import { usersProviders } from './users.providers';
import { UsersService } from './users.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserDocument }]),
  ],
  providers: [UsersService, ...usersProviders],
  exports: [UsersService],
})
export class UsersModule {}
