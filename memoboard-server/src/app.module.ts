import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { PostsModule } from './modules/posts/posts.module';
import { ImagesModule } from './modules/images/images.module';

@Module({
  imports: [ 
            ConfigModule.forRoot({ isGlobal: true }), 
            MongooseModule.forRoot(
              process.env.MONGO_URL, 
              {
                useCreateIndex: true,
                useUnifiedTopology: true
              }), 
            UsersModule,
            AuthModule,
            PostsModule,
            ImagesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
