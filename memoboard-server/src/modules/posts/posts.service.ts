import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { InjectModel, MongooseModule } from '@nestjs/mongoose';
import { Model, Mongoose, Types } from 'mongoose';
import { FilesService } from '../images/images.service';
import { PostDto } from './dto/post.dto';
import { Post } from './post.entity';

@Injectable()
export class PostsService {
    constructor(@InjectModel('Post') private readonly postModel: Model<Post>, private readonly fileService: FilesService){}

    async create(post: PostDto, userId): Promise<Post> {
        let newPost = new this.postModel(post);
        newPost.user = Types.ObjectId(userId);
        return newPost.save();
    }

    async findAllFromUser(userId): Promise<Post[]> {
        return this.postModel.find({user: Types.ObjectId(userId)}).sort('-date').select('-__v').exec();
    }

    async findOne(id): Promise<Post> {
        return this.postModel.findById(id).populate({path: 'user', select: '-password -__v'}).select('-__v').exec();
    }
    

    async delete(id: string)
    {

        const deleted = await this.postModel.findByIdAndDelete(id).exec();
        this.fileService.deleteFile(deleted.imageId);
        return deleted;
        
    }

    async update(id, data)
    {
        return await this.postModel.findByIdAndUpdate(id, {...data}, {new: true}).select('-__v').exec();
    }
}
