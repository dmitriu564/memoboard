import { Controller, Delete, Get, InternalServerErrorException, NotFoundException, Param, Post, Req, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileResponseVm } from './fileResponse.model';
import { FilesService } from './images.service';

@Controller('images')
export class ImagesController {
    constructor(private readonly filesService: FilesService){}

    @Post()
    @UseInterceptors(FileInterceptor('file'))
    upload(@UploadedFile() file, @Req() req)
    {
        return {id: file.id};
    }

    @Get(':id')
    async downloadFile(@Param('id') id: string, @Res() res) {
        const file = await this.filesService.findInfo(id)        
        const filestream = await this.filesService.readStream(id)
        if(!filestream){
            throw new NotFoundException('File not found');
        } 
        res.header('Content-Type', file.contentType);
        res.header('Content-Disposition', 'attachment; filename=' + file.filename);
        return filestream.pipe(res) 
    }
    @Delete(':id')
    async deleteFile(@Param('id') id: string): Promise<FileResponseVm> {
        const file = await this.filesService.findInfo(id)
        const filestream = await this.filesService.deleteFile(id)
        if(!filestream){
            throw new InternalServerErrorException('Failed to delete file');
        }        
        return {
            file: file
        }
    }
}
