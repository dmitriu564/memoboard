import { ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthGuard, PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { UsersService } from "../users/users.service";



@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy)
{
    constructor(private readonly userService: UsersService)
    {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: process.env.JWTKEY,
        });
    }
    
    async validate(payload: any)
    {
        const user = await this.userService.findOneById(payload._id);
        if(!user)
        {
            throw new UnauthorizedException('User is not authorized');
        }

        return payload;
    }
}

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(err, user, info) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}