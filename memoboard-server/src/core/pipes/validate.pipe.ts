import { ArgumentMetadata, BadRequestException, Injectable, UnprocessableEntityException, ValidationPipe } from "@nestjs/common";

interface ErrResponse
{
    message: string[],
}

@Injectable()
export class ValidateInputPipe extends ValidationPipe
{
    public async transform(value, metadata: ArgumentMetadata)
    {
        try
        {
            return await super.transform(value, metadata);
        }
        catch(e)
        {
            console.log(value, metadata);
            if(e instanceof BadRequestException)
            {
                throw new BadRequestException((e.getResponse() as ErrResponse).message);
            }
        }
    }
}