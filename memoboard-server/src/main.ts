import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as morgan from 'morgan';
import { ValidationError } from 'class-validator';
import { validationErrorHandler } from './core/handlers/validationError.handler';
//import { ValidateInputPipe } from './core/pipes/validate.pipe';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api/v1');
  app.useGlobalPipes(new ValidationPipe({
    exceptionFactory: validationErrorHandler
  }));
  app.enableCors(); //TODO: security
  app.use(morgan('tiny'));
  await app.listen(process.env.PORT || 5000);
}
bootstrap();
