import { USER_REPOSITORY } from '../../core/constants';
import { User } from './user.entity';
require('dotenv').config(); //bruh

export const usersProviders = [
  {
    provide: USER_REPOSITORY,
    useValue: User,
  },
];
