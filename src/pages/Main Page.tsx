import React, { useCallback, useContext, useEffect, useState } from 'react';
import { PostWithId } from '../components/MemoCard';
import { MemoList } from '../components/MemoList';
import { AuthContext } from '../context/auth.context';
import { useHttp } from '../hooks/http.hook';

export const MainPage = () => {
    const auth = useContext(AuthContext);

    const { loading, request } = useHttp();
    const [memos, setMemos] = useState<PostWithId[]>([]);


    const loadMemos = useCallback(async () => {
        try {
            const res = await request('/api/v1/posts', 'GET', null, {
                'Authorization': `Bearer ${auth.token}`
            });
            setMemos(res);
        } catch (e) {
            console.log(e);
        }
    }, [auth.token, request]);

    useEffect(() => {
        const top = document.querySelectorAll('#top-pics.carousel');
        M.Carousel.init(top, {
            fullWidth: true,
            indicators: true,
        });
        const gallery = document.querySelectorAll('#gallery.carousel');
        M.Carousel.init(gallery);

        loadMemos();

    }, [loadMemos]);

    const deleteHandler = useCallback(async (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, id: string) => {
        e.preventDefault();
        await request(`/api/v1/posts/${id}`, 'DELETE', null, {
            'Authorization': `Bearer ${auth.token}`
        });
        await loadMemos();
    }, [loadMemos, request, auth.token]);

    return (
        <>
            <div id="top-pics" className="carousel carousel-slider">
                <a className="carousel-item half-tall" href="#one!"><img alt="Carousel cool pic 1" src="https://lorempixel.com/1280/400/abstract/1" /></a>
                <a className="carousel-item" href="#two!"><img alt="Carousel cool pic 2" src="https://lorempixel.com/1280/400/abstract/2" /></a>
                <a className="carousel-item" href="#three!"><img alt="Carousel cool pic 3" src="https://lorempixel.com/1280/400/abstract/3" /></a>
                <a className="carousel-item" href="#four!"><img alt="Carousel cool pic 4" src="https://lorempixel.com/1280/400/abstract/4" /></a>
                <a className="carousel-item" href="#five!"><img alt="Carousel cool pic 5" src="https://lorempixel.com/1280/400/abstract/5" /></a>
            </div>

            <section className="container">
                <h2 className="center">
                    Your Memos
                </h2>
                <MemoList memos={memos} deleteHandler={deleteHandler} loading={loading} reloadHandler={loadMemos} />
            </section>
            <section className="container">
                <h2 className="center">
                    Gallery
                </h2>
                <div id="gallery" className="carousel force-300px padding-bottom">
                    <a className="carousel-item" href="#one!"><img src="https://lorempixel.com/250/350/abstract/1" /></a>
                    <a className="carousel-item" href="#two!"><img src="https://lorempixel.com/350/250/nature/2" /></a>
                    <a className="carousel-item" href="#three!"><img src="https://lorempixel.com/250/200/nature/3" /></a>
                    <a className="carousel-item" href="#four!"><img src="https://lorempixel.com/200/250/nature/4" /></a>
                    <a className="carousel-item" href="#five!"><img src="https://lorempixel.com/350/150/nature/5" /></a>
                </div>
            </section>
        </>
    )
}