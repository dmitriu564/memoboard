import { IsEmail, IsEnum, IsNotEmpty, MinLength } from "class-validator";

enum Gender {
  UNICORN = 'unicorn',
  FEMALE = 'female',
  MALE = 'male',
  OTHER = 'other',
}

export class UserDto {
  @IsNotEmpty()
  readonly name: string;
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
  @IsNotEmpty()
  @MinLength(8)
  readonly password: string;
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'gender must be male, female, other or unicorn'
  })
  readonly gender: string;
}
