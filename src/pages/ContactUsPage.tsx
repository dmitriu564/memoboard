import React from 'react';
import { Helmet } from 'react-helmet';
import { MapContainer, TileLayer, Marker } from 'react-leaflet';

export const ContactUsPage = () => {
    return (

        <div className="container">
            <Helmet>
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
                    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
                    crossOrigin="" />
                <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
                    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
                    crossOrigin=""></script>
            </Helmet>
            <h1 className="center">Contact Us</h1>
            <p>I'm here, maybe</p>
            <div>
                <Helmet>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
                        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
                        crossOrigin="" />
                </Helmet>
                <MapContainer className="map" center={[51.505, -0.09]} zoom={13} scrollWheelZoom={false}>
                    <TileLayer
                        url='https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}'
                        attribution='Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
                        maxZoom={18}
                        id='mapbox/streets-v11'
                        tileSize={512}
                        zoomOffset={-1}
                        accessToken='sk.eyJ1IjoiZG15dHIiLCJhIjoiY2tvbXo1bWt5MDNhbDJ1bXg4Mm0yaDBsNSJ9.DFQH0zPBEeDuQiFGFKGUcw'
                    />
                    
                </MapContainer>
            </div>

        </div>
    )
}