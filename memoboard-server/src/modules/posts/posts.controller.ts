import { Body, Controller, Get, Post, Put, Delete, NotFoundException, Param, UseGuards, Request, HttpCode } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ObjectId } from 'mongoose';
import { MongoIdDTO } from './dto/ObjectId.dto';
import { PostDto } from './dto/post.dto';
import { Post as PostEntity } from './post.entity';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {
    constructor(private readonly postService: PostsService){
    }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    async findAll(@Request() req)
    {
        return await this.postService.findAllFromUser(req.user._id);
    }

    @Get(':id')
    async findOne(@Param('id') id: string): Promise<PostEntity> {
        const post = await this.postService.findOne(id);

        if(!post)
        {
            throw new NotFoundException('This note doesn\'t exist yet');
        }

        return post;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post()
    async create(@Body() post: PostDto, @Request() req): Promise<PostEntity> {
        const newPost = await this.postService.create(post, req.user._id);
        return newPost;
    }

    @UseGuards(AuthGuard('jwt'))
    @Put(':id')
    async update(@Param('id') id: string, @Body() post: PostDto): Promise<PostEntity> {
        const updPost = await this.postService.update(id, post);
        if(!updPost)
        {
            throw new NotFoundException('This Post doesn\'t exist');
        }
        return updPost;
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete(':id')
    async remove(@Param('id') id: string) {
        const deleted = await this.postService.delete(id);

        if (!deleted) {
            throw new NotFoundException('This Post doesn\'t exist');
        }

        return {message: 'successful delete'};
    }
}
