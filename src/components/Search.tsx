import { useEffect } from "react";

interface SearchProps {
    searchHandler: (searchText: string) => void,
}

export const Search = ({ searchHandler }: SearchProps) => {
    const onSearchChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        searchHandler(e.currentTarget.value);
    }

    useEffect(() => {
        M.updateTextFields()
    }, [])

    return (
        <form>
            <div className="input-field">
                <input id="search" type="search" required onChange={onSearchChange} onLoad={onSearchChange}/>
                <label htmlFor="search">Search</label>
            </div>
        </form>
    );
}