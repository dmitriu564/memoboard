import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Mongoose, Types } from 'mongoose';
import { User } from '../users/user.entity';

export type PostDocument = Post & Document;

@Schema({ timestamps: true })
export class Post {
  @Prop({ required: true })
  title: string;

  @Prop()
  description: string;

  @Prop()
  date: Date;

  @Prop({type: Types.ObjectId, ref: 'User'})
  user: Types.ObjectId;

  @Prop()
  imageId: string; //pain, pain, pain
}


export const PostDocument = SchemaFactory.createForClass(Post);
