import { Injectable } from "@nestjs/common";
import { MulterModuleOptions, MulterOptionsFactory } from "@nestjs/platform-express";
import { rejects } from "assert";
import * as GridFsStorage from 'multer-gridfs-storage'
@Injectable()
export class GridFsMulterConfigService implements MulterOptionsFactory{
    gridFsStorage: GridFsStorage;
    constructor(){
        this.gridFsStorage = new GridFsStorage({
            url: process.env.MONGO_URL,
            options: {
                useCreateIndex: true,
                useUnifiedTopology: true
              },
            file: (req, file) => {
                if (file.mimetype === 'image/jpeg') {
                  return {
                    bucketName: 'images'
                  };
                }
            }

        });
    }

    createMulterOptions(): MulterModuleOptions{
        return {
            storage: this.gridFsStorage,
        }
    }
}