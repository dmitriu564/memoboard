import { useCallback, useState } from "react"


export interface ValidationError{
    [propName: string]: string[];
}

export const useHttp = () => {
    const [loading, setLoading] = useState(false);
    const [err, setErr] = useState<ValidationError | Error | null>(null);

    const request = useCallback(async (url: string, method: string='GET', body: any=null, headers?: HeadersInit) => {
        setLoading(true);
        try {
            if(headers && (headers as Record<string, string>)['Content-Type'] ==='application/json')
            {
                body = JSON.stringify(body);
            }

            const res = await fetch(url, {method, body, headers});
            if(res.headers.get('transfer-encoding') === 'chunked')
                return res;
            const data = await res.json();

            if(!res.ok)
            {
                throw data || (new Error('Request failed'));
            }
            setLoading(false);
            return data;
            
        } catch (e) {
            setLoading(false);
            setErr(e);
            console.log(e);
            //throw e;
        }
    }, []);

    const cleanErr = () => setErr(null);

    return {loading, err, request, cleanErr};
}