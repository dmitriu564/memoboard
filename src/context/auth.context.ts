import { createContext } from "react";
import { User } from "../hooks/auth.hook";

interface AuthContextProps{
    token: string | null,
    user: User | null,
    login: Function,
    logout: Function,
    isAuthed: boolean
}

export const AuthContext = createContext<AuthContextProps>({
    token: null,
    user: null,
    login: Function(),
    logout: Function(),
    isAuthed: false
});