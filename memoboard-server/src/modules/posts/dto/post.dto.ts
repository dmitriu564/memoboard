import { IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class PostDto {
    @IsNotEmpty()
    @MinLength(5)
    @MaxLength(128)
    title: string;

    @MinLength(3)
    @MaxLength(256)
    description: string;

    @IsNotEmpty()
    date: Date;
}