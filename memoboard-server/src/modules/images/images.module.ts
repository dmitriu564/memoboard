import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { GridFsMulterConfigService } from './gridFsMulterConfig.service';
import { ImagesController } from './images.controller';
import { FilesService } from './images.service';

@Module({
    imports: [
        MulterModule.registerAsync(
          { useClass: GridFsMulterConfigService}),],
    providers: [FilesService, GridFsMulterConfigService],
    controllers: [ImagesController]
})
export class ImagesModule {}
