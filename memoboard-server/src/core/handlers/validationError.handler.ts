import { BadRequestException } from "@nestjs/common";
import { ValidationError } from "class-validator";

export const validationErrorHandler = (errors: ValidationError[]) => 
{

  return new BadRequestException(errors.reduce((a, e) => ({ 
    ...a, [e.property]: Object.values(e.constraints)
  }), {}));
}