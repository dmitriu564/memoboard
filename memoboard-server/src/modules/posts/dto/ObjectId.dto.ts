import { IsMongoId } from "class-validator";
import { ObjectId } from "mongoose";

export class MongoIdDTO {
  @IsMongoId()
  id: ObjectId;
}
