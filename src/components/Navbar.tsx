import React, { MouseEventHandler, useContext, useEffect } from 'react'
import { NavLink, useHistory } from 'react-router-dom';
import { AuthContext } from '../context/auth.context';
import { Helmet } from "react-helmet";



export const Navbar = () => {
    const auth = useContext(AuthContext);
    const hist = useHistory()
    const logoutHandler: MouseEventHandler<HTMLAnchorElement> = (e) => {
        e.preventDefault();
        auth.logout();
        hist.push('/');
    }
    useEffect(() => {
        const dropdown = document.getElementsByClassName('dropdown-trigger')[0];
        M.Dropdown.init(dropdown);
    }, []);
    //TODO: change second dropdown link to FAQ
    return (
        <div>
            <ul id="dropdown-about" className="dropdown-content">
                <li><NavLink to="/contactus" className="black-text">Contact Us</NavLink></li>
                <li><NavLink to="/about" className="black-text">What is this?</NavLink></li> 
            </ul>
            <div className="navbar-fixed">
                <nav>
                    <div className="nav-wrapper blue-grey lighten-2">
                        <a href="/" className="brand-logo center black-text hide-on-med-and-down">Memoboard</a>
                        <ul id="nav" className="right separate-vert">
                            <li><NavLink to="/events" className="black-text">Memos</NavLink></li>
                            <li>
                                <a className="dropdown-trigger black-text" href="#!" data-target="dropdown-about">
                                    About<i className="material-icons right">arrow_drop_down</i>
                                </a>
                                <Helmet>
                                    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
                                </Helmet>
                            </li>
                            <li><a href="/#" onClick={logoutHandler} className="black-text">Log out</a></li>
                        </ul>
                    </div>
                </nav>
            </div>

        </div>
    );
}