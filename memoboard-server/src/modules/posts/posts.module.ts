import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostDocument } from './post.entity';
import { postsProviders } from './posts.provider';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { FilesService } from '../images/images.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Post', schema: PostDocument }]),],
  providers: [PostsService, FilesService],
  controllers: [PostsController]
})
export class PostsModule {}
