import { useCallback, useEffect, useState } from "react";
import { useHttp } from "../hooks/http.hook";

export interface Post {
    title: string;
    description: string;
    date: string;
    imageId?: string;
}


export interface PostWithId extends Post {
    _id: string;
}



interface MemoCardProps {
    memo: PostWithId;
    onDelete: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, id: string) => void;
    loading: boolean;
}

export const MemoCard = ({ memo, onDelete, loading }: MemoCardProps) => {
    const [url, setUrl] = useState('');
    const { request, err } = useHttp();
    const loadImg = useCallback(async () => {
        try {
            if (memo.imageId) {
                console.log(memo);
                const img = await(await request(`api/v1/images/${memo.imageId}`, 'GET')).blob();
                setUrl(URL.createObjectURL(img));
            }
        } catch (e) {
            console.log(e.message);
        }

    }, [request, memo.imageId]);
    useEffect(() => {
        loadImg();
    }, [loadImg]);

    const isDueMemo = Date.now() > Date.parse(memo.date);
    return (
        <div className="col">
            <div className="col s12 m6">
                <div className={"card " + (isDueMemo ? "blue-grey darken-1" : "green darken-1")}>
                    {url && <div>
                        <img className="crop" src={url} />
                    </div>}
                    <div className="card-content white-text">
                        <span className="card-title truncate">{memo.title}</span>
                        <p>Description: {memo.description}</p>
                        <p>{isDueMemo ? "Happened at: " : "Due to: "} {memo.date}</p>
                    </div>
                    <div className="card-action">
                        <a onClick={(e) => { if (!loading) onDelete(e, memo._id) }} href="/#">Remove</a>
                    </div>
                </div>
            </div>
        </div>
    );
}