import { useState } from "react";
import * as _ from 'lodash';

interface PaginationProps {
    totalPages: number;
    onSetPage: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, page: number) => void;
}

export const PaginationRow = ({totalPages, onSetPage}: PaginationProps) => {
    const [page, setPage] = useState(1);

    const pageChangeHandler = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, newPage: number) => {
        if(newPage <= totalPages && newPage > 0){
            setPage(newPage);
            onSetPage(e, newPage);
        }
    };

    return (
        <ul className="pagination">
            <li className={page===1?"disabled":"waves-effect"}>
                <a onClick={(e) => { pageChangeHandler(e, page-1);}} href="#!">
                    <i className="material-icons">chevron_left</i>
                </a>
            </li>
            {_.times(totalPages, (i) => { //index key is kinda legal here, pagination pages won't randomly change order and always represent the same page activity-wise
                return <li key={i} className={(i+1)===page?"active":"waves-effect"}><a onClick={(e) => {pageChangeHandler(e, (i+1))}} href="#!">{i+1}</a></li>
            })}
            <li className={page===totalPages?"disabled":"waves-effect"}>
                <a onClick={(e) => { pageChangeHandler(e, page+1);}} href="#!">
                    <i className="material-icons">chevron_right</i>
                </a>
            </li>
        </ul>
    );
}